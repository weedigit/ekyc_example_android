package wee.dev.examplelibface.main

import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.os.Bundle
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.core.text.HtmlCompat
import kotlinx.android.synthetic.main.activity_main.*
import wee.dev.examplelibface.R
import wee.dev.examplelibface.face.FaceActivity
import wee.dev.examplelibface.pin.CreatePinActivity
import wee.dev.examplelibface.pin.VerifyPinActivity

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initView()
    }

    private fun initView() {

        val textColor = getString(
            R.string.main_text,
            "<font color='#212121'><br>YOUR</font>",
            "<font color='#33ADFF'><br>FUTURE</font>",
            "<font color='#33ADFF'>.</font>"
        )
        actMain_labelTitle.text = HtmlCompat.fromHtml(textColor, HtmlCompat.FROM_HTML_MODE_LEGACY)

        actMain_labelCamera.setOnClickListener {
            startActivity(Intent(this, FaceActivity::class.java))
            finish()
        }

        actMain_labelRegisterPinCode.setOnClickListener {
            startActivity(Intent(this, CreatePinActivity::class.java))
        }

        actMain_labelVerifyPinCode .setOnClickListener {
            startActivity(Intent(this, VerifyPinActivity::class.java))
        }
    }

}