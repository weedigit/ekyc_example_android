package wee.dev.examplelibface.pin

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_result_pin.*
import wee.dev.examplelibface.R
import wee.dev.examplelibface.main.MainActivity

class ResultPinActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result_pin)

        val pinCode = intent.extras?.get("pincode").toString()
        actResultPin_labelPin.text = "$pinCode"

        actResultPin_action.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }

        actResultPin_toolbar.goneActionBack()

    }

}