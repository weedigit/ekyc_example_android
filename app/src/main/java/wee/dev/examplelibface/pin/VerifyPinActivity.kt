package wee.dev.examplelibface.pin

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_verify_pin.*
import wee.dev.examplelibface.R
import wee.dev.libface.util.Language

class VerifyPinActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_pin)

        actVerifyPin_pinCode.initPinCodeCallBack(
            wee.dev.libface.control.PinCodeView.PIN_VERIFY,
            Language.LANGUAGE_VN,
            object : wee.dev.libface.control.PinCodeView.PinCodeCallBack {
                override fun onResult(pin: String, confirmPin: String) {
                    val intent = Intent(this@VerifyPinActivity, ResultPinActivity::class.java)
                    intent.putExtra("pincode", pin)
                    startActivity(intent)
                    finish()
                }
            })

        actVerifyPin_pinCode.onBackClickListener(View.OnClickListener {
            finish()
        })
    }

    override fun onResume() {
        super.onResume()
        actVerifyPin_pinCode.resumePinCode()
    }

    override fun onPause() {
        super.onPause()
        actVerifyPin_pinCode.pausePinCode()
    }

}