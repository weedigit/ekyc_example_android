package wee.dev.examplelibface.face

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_result_face.*
import wee.dev.examplelibface.R
import wee.dev.examplelibface.main.MainActivity

class ResultFaceActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_result_face)
        initView()
    }

    private fun initView() {

        if (FaceActivity.isCheckResult) {
            actResultFace_image.setImageBitmap(FaceActivity.userImage)

            actResultFace_toolbar.actionBack(View.OnClickListener {
                startActivity(Intent(this, FaceActivity::class.java))
                finish()
            })

            actFaceSuccess_action.setOnClickListener {
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
        } else {
            actResultFace_image.setImageResource(R.drawable.ic_fail)
            actResultFace_image.borderWidth = 0

            actResultFace_icSuccess.visibility = View.GONE

            actFaceSuccess_action.text = "Thử lại"
            actFaceSuccess_action.setOnClickListener {
                startActivity(Intent(this, FaceActivity::class.java))
                finish()
            }

            actResultFace_toolbar.goneActionBack()

        }
    }

    override fun onDestroy() {
        FaceActivity.userImage?.recycle()
        super.onDestroy()
    }
}
