package wee.dev.examplelibface.face

import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_face.*
import wee.dev.examplelibface.R
import wee.dev.examplelibface.main.MainActivity
import wee.dev.libface.base.LiveNessCheckData
import wee.dev.libface.control.EkycFaceView
import wee.dev.libface.util.Language

class FaceActivity : AppCompatActivity(), EkycFaceView.EkycFaceViewCallBack {

    companion object {
        const val REQUEST_PERMISSION_CODE = 100

        const val CAMERA_PERMISSION = android.Manifest.permission.CAMERA

        var userImage: Bitmap? = null

        var isCheckResult: Boolean = false
    }

    private var isPermission = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_face)
        checkPermissionCamera()

        actMain_ekycFaceControl.onBackClickListener(View.OnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        })

    }

    private fun initControl(){
        actMain_ekycFaceControl.initLibrary(this, Language.LANGUAGE_VN)
    }

    private fun checkPermissionCamera(){
        isPermission = checkSelfPermission(CAMERA_PERMISSION) == PackageManager.PERMISSION_GRANTED
        if(!isPermission) {
            requestPermissions(
                arrayOf(CAMERA_PERMISSION),
                REQUEST_PERMISSION_CODE
            )
        }else{
            initControl()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == REQUEST_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) initControl()
        }
    }

    override fun onComplete(data: LiveNessCheckData) {
        if (data.faceData == null) return
        val bitmap = BitmapFactory.decodeByteArray(data.faceData, 0, data.faceData!!.size)
        actionDone(bitmap, data.status)
    }

    private fun actionDone(bitmap: Bitmap?, isLiveness: Boolean) {
        isCheckResult = isLiveness
        userImage = bitmap
        startActivity(Intent(this, ResultFaceActivity::class.java))
        finish()
    }

    override fun onPause() {
        super.onPause()
        actMain_ekycFaceControl.onPauseLibrary()
    }

    override fun onResume() {
        super.onResume()
        actMain_ekycFaceControl.onResumeLibrary()
    }

    override fun onDestroy() {
        super.onDestroy()
        actMain_ekycFaceControl.onDestroyLibrary()
    }
}
