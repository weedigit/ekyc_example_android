package wee.dev.examplelibface.custom

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.toolbar_view.view.*
import wee.dev.examplelibface.R

class Toolbar : ConstraintLayout{

    constructor(context: Context) : super(context) {
        initView(context, null)
    }

    constructor(context: Context, attributeSet: AttributeSet?) : super(context, attributeSet) {
        initView(context, attributeSet)
    }

    constructor(context: Context, attributeSet: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attributeSet,
        defStyleAttr
    ) {
        initView(context, attributeSet)
    }

    private fun initView(context: Context, attributeSet: AttributeSet?) {
        LayoutInflater.from(context).inflate(R.layout.toolbar_view, this)

        toolbarView_actionBack.visibility = View.VISIBLE
    }

    fun actionBack(listener : OnClickListener){
        toolbarView_actionBack.setOnClickListener {
            listener.onClick(it)
        }
    }

    fun goneActionBack(){
        toolbarView_actionBack.visibility = View.GONE
    }

}