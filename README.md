# EKYC #

Weedigital's liveness check android library

### Download

* request Gradle : minSdkVersion 23

Gradle:

```gradle
repositories {
	maven { url 'https://jitpack.io' }
        maven {
            url "https://mymavenrepo.com/repo/319jIgR9UdosJWUWFo6K/"
            credentials {
                username = 'myMavenRepo'
                password = 'weedigital'
            }
        }
	}
}

 defaultConfig {
    renderscriptTargetApi 29
    renderscriptSupportModeEnabled true
 }

dependencies {
  	implementation 'wee.dev.libface:android-library:V1.0.10'
    implementation 'libface:ekyc:V2.0.0'
}
```

* You need to visit https://console.firebase.google.com/ to sign up for google service and import google_service.json file into your project

### How do I get set up EKYC LiveNess Face? ###



* view
```xml
 <wee.dev.libface.control.EkycFaceView
     android:id="@+id/actMain_ekycFaceControl"
     android:layout_width="match_parent"
     android:layout_height="match_parent"
     app:layout_constraintBottom_toBottomOf="parent"
     app:layout_constraintEnd_toEndOf="parent"
     app:layout_constraintStart_toStartOf="parent"
     app:layout_constraintTop_toTopOf="parent" />
```


* you set a listener, language to your view in Activity.onCreate or Fragment.onViewCreate (Note: you should ask for application's camera permission before initLibrary)

* option language :
+ VIETNAM : wee.dev.libface.util.Language.Language.LANGUAGE_VN
+ ENGLISH : wee.dev.libface.util.Language.Language.LANGUAGE_EN

```kotlin
    ekycFaceView.initLibrary(object : EkycFaceView.EkycFaceViewCallBack {
        override fun onComplete(data: LiveNessCheckData) {
            val faceData: ByteArray? = data.faceData
        }
    }, Language.LANGUAGE_VN)
```



* EKYC's lifecycle management in onPause, onResume, onDestroy Activity or Fragment
```kotlin
    override fun onResume() {
        super.onResume()
        actMain_ekycFaceControl.onResumeLibrary()
    }
    
    override fun onPause() {
        super.onPause()
        actMain_ekycFaceControl.onPauseLibrary()
    }

    override fun onDestroy() {
        super.onDestroy()
        actMain_ekycFaceControl.onDestroyLibrary()
    }
```

### How do I get set up EKYC Pin Code? ###

* view
```xml
 <wee.dev.libface.control.PinCodeView
        android:id="@+id/actPin_pinCode"
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        app:layout_constraintBottom_toBottomOf="parent"
        app:layout_constraintEnd_toEndOf="parent"
        app:layout_constraintStart_toStartOf="parent"
        app:layout_constraintTop_toTopOf="parent" />
```


* you set a listener, type PinCode, language to your view in Activity.OnCreate or Fragment.onViewCreate

* option language :
+ VIETNAM : wee.dev.libface.util.Language.Language.LANGUAGE_VN
+ ENGLISH : wee.dev.libface.util.Language.Language.LANGUAGE_EN

+ Create Pin Code : wee.dev.libface.control.PinCodeView.PIN_CREATE
 
+ Verify Pin Code : wee.dev.libface.control.PinCodeView.PIN_VERIFY

```kotlin
        actCreatePin_pinCode.initPinCodeCallBack(
            wee.dev.libface.control.PinCodeView.PIN_CREATE,
            Language.LANGUAGE_VN,
            object : wee.dev.libface.control.PinCodeView.PinCodeCallBack {
                override fun onResult(pin: String, confirmPin: String) {
                    Toast.makeText(this@CreatePinActivity, "$pin - $confirmPin", Toast.LENGTH_SHORT)
                        .show()
                }
            })

       actCreatePin_pinCode.onBackClickListener(View.OnClickListener {
            finish()
        })
```

* Pin Code lifecycle management in onPause, onResume


```kotlin
    override fun onResume() {
        super.onResume()
        actCreatePin_pinCode.resumePinCode()
    }

    override fun onPause() {
        super.onPause()
        actCreatePin_pinCode.pausePinCode()
    }
```

### WEEDIGITAL COMPANY ###


![](weedigital.png)


* Address: No. B20 Bach Dang, Tan Binh District, Ho Chi Minh City, Vietnam

* Email:  Weedigital@wee.vn